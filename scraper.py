#!/usr/bin/env python
import sys
sys.path.append('./lib')
import MyTest, lib
import requests, re, json
from lxml import html

## Product class represents product information
class product:
  ## Cache of values, reduce network
  link         = None
  title        = None
  unit_price   = None
  target_doc   = None
  size         = None
  description  = None

  def __init__(self, elem, debug=False):
    self.elem = elem
    self.debug = debug

  # Returns link for full product page
  def getLink(self):
    if (self.link !=None):
      return self.link
    self.link = self.elem.find('.//div[@class="productInfo"]/h3/a').attrib["href"].strip()
    return self.link

  # Grabs title from product element
  def getTitle(self):
    if (self.title !=None):
      return self.title
    self.title = self.elem.find('.//div[@class="productInfo"]/h3/a').text.strip()
    return self.title

  # Unit price int double format from product information
  def getUnitPrice(self):
    if (self.unit_price !=None):
      return self.unit_price
    string = self.elem.find('.//p[@class="pricePerUnit"]').text
    match = re.search('[0-9]+\.[0-9]{2}', string)
    if match == None:
      return -1
    self.unit_price = match.group(0)
    return self.unit_price

  # Retrieve document size from target page header
  def getSize(self):
    if (self.size!=None):
      return self.size
    if (self.target_doc == None):
      self.target_doc = handleRequest(self.getLink(), debug=self.debug)
    self.size = lib.convertBytes(self.target_doc.headers['content-length'])
    return self.size

  # Retrieve item description from target page
  def getDescription(self):
    if (self.description!=None):
      return self.description
    if (self.target_doc == None):
      self.target_doc = handleRequest(self.getLink(), debug=self.debug)
    itemtree = html.fromstring(self.target_doc.content)
    self.description = " ".join(itemtree.xpath("//productcontent/htmlcontent/div[@class='productText'][1]/descendant::*/text()")).strip()
    return self.description

  # Represent product properties as a dictionary
  def getDict(self):
    result = {
      'title' : self.getTitle(),
      'size'  : self.getSize(),
      'unit_price': self.getUnitPrice(),
      'description': self.getDescription()
    }
    return result;

# Convert plain string to xml tree
def parseDoc(content):
  return html.fromstring(content)

# Handle HTTP request
# If debug = True, use local file system - reduce network traffic
def handleRequest(loc, debug=False):
  if debug == True:
    with open("./5_products.html") as f:
      result = MyTest.fakePage(f.read(), {'content-length':"800"})
  else:
    result = requests.get(loc)
  return result

# Iterate through the products found in the page and add to list
def getProducts(tree, debug = False):
  result = {
    "results" : []
  }
  total = 0.0
  for elem in tree.xpath('//div[@class="product "]'):
    #elem = tree.xpath('//div[@class="product "]')[0]
    elem = product(elem, debug = debug)
    item = elem.getDict()
    total += float(item['unit_price'])  # Maintain running list of total unit prices
    result['results'].append(item)
  result['total'] = "%.2f" % (total)
  return result

## Runs through the crawl in debug mode - reduce network traffic
def test():
  page = handleRequest("./5_products.html",debug = True)
  tree = parseDoc(page.content)
  products = getProducts(tree, debug = True)
  #print json.dumps(products, sort_keys=True, indent=4, separators=(',', ': '))
  MyTest.test("test 1: results object"            , "results" in products   , True)
  MyTest.test("test 2: results count"             , len(products['results']), 7)
  MyTest.test("test 3: each item has description" , [1 if "description" in i else 0 for i in products['results']].count(1), 7)
  MyTest.test("test 4: each item has size"        , [1 if "size" in i else 0 for i in products['results']].count(1), 7)
  MyTest.test("test 5: each item has title"       , [1 if "title" in i else 0 for i in products['results']].count(1), 7)
  MyTest.test("test 6: each item has unit_price"  , [1 if "unit_price" in i else 0 for i in products['results']].count(1), 7)
  MyTest.test("test 7: running total"             , ("total" in products), True)

def run(target):
  page = handleRequest(target)
  tree = parseDoc(page.content)
  products = getProducts(tree)
  print json.dumps(products, sort_keys=True, indent=4, separators=(',', ': '))

if __name__ == "__main__":
  # If the 'd' parameter is supplier, starts in debug mode
  if (len(sys.argv) == 1 or "d" in sys.argv):
    print "**debug**"
    test()
    sys.exit(0)
  # Pass URL to run function
  run(sys.argv[-1])