#Sainsbury's Web Scraper
Mark Greenwood - 23/04/2016

##Dependencies

Written for Python 2.7

`pip install lxml`

`pip install requests`

##To run

`./scraper.py {d|url}`

d   - switches debug mode.  Local requests only
url - the URL tp scrape
