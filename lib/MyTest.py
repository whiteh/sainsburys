class fakePage:
  def __init__(self, content, headers):
    self.content = content
    self.headers = headers
    
def test(n, val, expected):
  try:
    assert (val == expected)
    print "OK : " + str(n)+ ": expected "+str(expected)+", got "+str(val)
    return True;
  except AssertionError:
    print "NOK: " + str(n)+ ": expected "+str(expected)+", got "+str(val)
    return False;
