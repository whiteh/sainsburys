def convertBytes(bytes):
  unit = ['b', 'kb', 'mb', 'gb']
  a = 0
  bytes = float(bytes)
  while bytes>1024:
    bytes = bytes/1000.0
    a+=1
  return "%.1f%s" % (bytes, unit[a])